import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyCN29MxlMn8kzKOWTYOIOXLUmecYSNNmos",
    authDomain: "uraan1312.firebaseapp.com",
    projectId: "uraan1312",
    storageBucket: "uraan1312.appspot.com",
    messagingSenderId: "962808644846",
    appId: "1:962808644846:web:08d807192d19a7b64d1fff"
  };

  firebase.initializeApp(firebaseConfig);

  export default firebase;
