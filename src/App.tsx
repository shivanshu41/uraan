import React from 'react';
import logo from './logo.svg';
import './App.css';
import Home from './components/Home';
import Login from './components/Login';
import SignUpPage from './components/Signup';
import Survey from './components/Survey';
import SurveyDetails from './components/SurveyDetails';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/home">
          <Home />
        </Route>
        <Route exact path="/">
          <div className='App'>
            <div className='inner' style={{ height: 'auto', overflow: "hidden" }}>
              <Login />
            </div>
          </div>
        </Route>
        <Route exact path="/signup">
          <div className='App'>
            <div className='inner'>
              <SignUpPage />
            </div>
          </div>
        </Route>
        <Route exact path="/surveydetails">
          <SurveyDetails />
        </Route>
        <Route exact path="/survey">
          <div className="App" >
            <div className="inner" style={{ maxWidth: "80%" }}>
              <Survey />

            </div>
          </div>

        </Route>
      </Switch>
    </Router>
  );
}

export default App;
