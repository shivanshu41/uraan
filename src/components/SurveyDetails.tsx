import React, { useEffect, useState } from 'react';
import firebase from '../firebase';
import { Table ,Button} from 'react-bootstrap';
import {useHistory} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
const db = firebase.database();
const SurveyDetails: React.FC = () => {
    const history = useHistory();
    const [surveyData, setSurveyData] = useState<any>([]);
    useEffect(() => {
        db.ref('survey')
            .on('child_added', (snapshot) => {
                if (snapshot.exists()) {
                    console.log(snapshot.key);
                    console.log(snapshot.val());
                    let data = snapshot.val();
                    setSurveyData((prevState:any)=>{
                        return prevState.concat({
                            name:data.name,
                            age:data.age,
                            occupation : data.occupation,
                            education : data.education,
                            address : data.address
                        })
                    })
                } else {
                    console.log("No survey data availble");
                    alert("No survey data availble Please add");
                }
            })

    }, [])

    return <>
        <Table striped bordered responsive hover variant = {'dark'} style={{marginTop:"5em"}}> 
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Age</th>
                    <th>Occupation</th>
                    <th>Education</th>
                </tr>
            </thead>
            <tbody>
                {surveyData.map((ele:any,index:any)=>{
                       return <tr>
                           <td>{ele.name}</td>
                           <td>{ele.address}</td>
                           <td>{ele.age}</td>
                           <td>{ele.occupation}</td>
                           <td>{ele.education}</td>
                       </tr> 
                })}
            </tbody>
        </Table>
        <Button style ={{backgroundColor:"red",background:"red",padding:"1em"}}
        onClick={(e)=>{
            firebase.auth().signOut();
            history.push('/')
        }}
        >Logout</Button>
    </>
}

export default SurveyDetails;