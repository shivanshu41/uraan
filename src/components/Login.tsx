import React, { useEffect, useState } from 'react';
import firebase from '../firebase';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import {useHistory ,Link} from 'react-router-dom';
const LoginPage: React.FC = () => {
    useEffect(()=>{
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
        
              console.log(user);
              window.location.href = '/home'
              // ...
            } else {
                console.log("No user found");
                }
          });
    },[])
    const [email, setEmail] = useState<string | null>(null);
    const [pass, setPass] = useState<string | null>(null);
    const [login,setLogin] = useState<boolean>(false);
    const history = useHistory();
    const handleLogin = (e:React.SyntheticEvent)=>{
        e.preventDefault();
        firebase.auth().signInWithEmailAndPassword(email!,pass!)
        .then((res)=>{
            console.log("Successfully signed in");
            history.push('/home');
        })
        .catch((e)=>{
            console.log(e);
            alert(`${e.message}`);
        })
    }
    return <>
        <h1>Login</h1>
        <Form onSubmit={(e)=>handleLogin(e)}>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control required type="email"
                    onChange={(e) => setEmail(e.currentTarget.value)} 
                    placeholder="Enter email" />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
    </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password"
                required
                    onChange={(e) => setPass(e.currentTarget.value)}
                    placeholder="Password" />
            </Form.Group>
            <Form.Group controlId="formBasicCheckbox">
                <Form.Check type="checkbox" label="Check me out" />
            </Form.Group>
            <Button variant="primary" type="submit">
                Log me in
            </Button>
        </Form>
        <div style={{marginTop:"1em"}}>
            Register <Link to={'/signup'}>Here</Link>
        </div>
    </>
}

export default LoginPage