import React, { useState } from 'react';
import { Form, Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import firebase from '../firebase';
import {
    Link
  } from "react-router-dom";
const db = firebase.database();
const SignUpPage: React.FC = () => {
    const [gender, setGender] = useState<string | null>(null);
    const [name, setName] = useState<string | null>(null);
    const [email, setEmail] = useState<string | null>(null);
    const [pass, setPass] = useState<string | null>(null);
    const [age, setAge] = useState<Number | null>(null);
    const [phone, setPhone] = useState<string | null>(null);
    const [father, setFather] = useState<string | null>(null);
    const [mother, setMother] = useState<string | null>(null);
    const [signup, setSignup] = useState<boolean | null>(null);
    const handleAuthSignup = (e?:React.SyntheticEvent) => {
        e?.preventDefault();
        firebase.auth().createUserWithEmailAndPassword(email!, pass!)
            .then((res) => {
                console.log("Successfully signed up to firebase");
                setSignup(true);
                handleSignup();
            })
            .catch((e) => {
                alert(`${e.message}`);
                console.log(e);
                setSignup(false);
            })
    }
    const handleSignup = (e?: React.SyntheticEvent) => {
        if(pass!.length < 6){
            alert("Password Should be atleast 6 characters long");
        }
        db.ref(`users/${name}`).set({
            name: name,
            email: email,
            phone: phone,
            age: age,
            father: father,
            mother: mother
        })
            .then((res) => {
                console.log("Successfull entry to db");
                // handling signup on firebase
            })
            .catch((e) => {
                console.log(e);
            })
    }
    const preRender= ()=>{
        if(signup){
           return  <div className={'successBox'}>
                SuccessFully signed up . Please <Link to={'/'}>Login Here</Link>
            </div>
        }else{
            return <> 
            <Form onSubmit={(e) => handleAuthSignup(e)}>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text"
                    required
                    onChange={(e) => setName(e.currentTarget.value)}
                    placeholder="Enter your name" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email"
                    required
                    onChange={(e) => setEmail(e.currentTarget.value)}
                    placeholder="Enter email"
                />
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
            </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicPassword">
                <Form.Label>Create Password</Form.Label>

                <Form.Control type="password"
                    required
                    onChange={(e) => {
                        setPass(e.currentTarget.value)
                        
                    }}
                    placeholder="Create Password" />
                <Form.Text className="text-muted">
                    Password should be atleast 6 characters
            </Form.Text>
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Age</Form.Label>
                <Form.Control type="number"
                    required
                    onChange={(e) => {
                        let intAge = parseInt(e.currentTarget.value);
                        setAge(intAge);
                    }}
                    maxLength={3} placeholder="Enter your age" />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Gender</Form.Label>
                <Form.Check
                    type={'radio'}
                    id={`default-radio`}
                    label={`Male`}
                    name={'male'}
                    value={'male'}
                    checked={gender === 'male'}
                    onChange={(e) => setGender(e.currentTarget.value)}
                    required={gender === 'male'}

                />
                <Form.Check
                    type={'radio'}
                    id={`default-radio`}
                    label={`Female`}
                    name={'female'}
                    value={'female'}
                    checked={gender === 'female'}
                    onChange={(e) => setGender(e.currentTarget.value)}
                    required={gender === 'female'}
                />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Father's Name</Form.Label>
                <Form.Control type="text"
                    required onChange={(e) => setFather(e.currentTarget.value)}
                    placeholder="Enter father's name" />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label> Mother's Name</Form.Label>
                <Form.Control type="text"
                    required onChange={(e) => setMother(e.currentTarget.value)}
                    placeholder="Enter your mother's name" />
            </Form.Group>

            <Form.Group controlId="formBasicEmail">
                <Form.Label>Your Phone number</Form.Label>

                <Form.Control type="tel"
                    required onChange={(e) => setPhone(e.currentTarget.value)}
                    maxLength={10} pattern={'^[6789][0-9]{9}'} placeholder="Enter your phone number"

                />
                <Form.Text className="text-muted">
                    it should be a valid indian phone number
            </Form.Text>
            </Form.Group>

            <Button variant="primary" type="submit">
                Sign Me Up
            </Button>
        </Form>
        
        <div style={{marginTop:"1em"}}>
            Already rsigned up ? Login <Link to={'/'}>Here</Link>
        </div>
        </>
        }
    }
    return <>
        <h1>Sign Up</h1>
        {preRender()}
    </>
}

export default SignUpPage;