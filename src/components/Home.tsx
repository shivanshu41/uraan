import React ,{useEffect} from 'react';
import firebase from '../firebase';
import SurveyDetails from './SurveyDetails';
import {useHistory} from 'react-router-dom';
import {Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
const Home:React.FC = ()=>{
    const history = useHistory();
    useEffect(()=>{
        firebase.auth().onAuthStateChanged((user) => {
            if (user) {
        
              console.log(user);
              // ...
            } else {
              window.location.href = '/'
            }
          });
    },[])
    return <>
       
        <Button style={{position:'absolute',top:'0',right:'0'}} variant = {'primary'} onClick={(e)=>{
            history.push('/survey');
        }}>+</Button>
        <SurveyDetails/>
    </>
}

export default Home;