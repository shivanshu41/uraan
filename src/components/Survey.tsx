import React , {useState} from 'react';
import { Form , Button } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import firebase from '../firebase';
const db = firebase.database();
const SurveyPage: React.FC = () => {
    const [name,setName] = useState("");
    const [add,setAdd] = useState("");
    const [age,setAge] = useState<Number | null>(null);
    const [occupation,setOccupation] = useState("");
    const [education,setEducation] = useState("");
    const handleSurvey = (e:React.SyntheticEvent)=>{
        e.preventDefault();
        db.ref('survey')
        .push({
            name:name,
            age:age,
            occupation:occupation,
            education:education,
            address:add
        })
        .then((res)=>{
            alert("Survey Finished and successfully submitted");
            console.log("Survey successfully added to Database");
        })
        .catch((e)=>{
            console.log(e);
        })
    }
    return <>
    <h1>Survey</h1>
        <Form onSubmit={(e)=>handleSurvey(e)}>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text" 
                onChange={(e)=>setName(e.currentTarget.value)}
                placeholder="Enter name" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Address</Form.Label>
                <Form.Control type="text" 
                onChange={(e)=>setAdd(e.currentTarget.value)}
                placeholder="Enter address" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Age</Form.Label>
                <Form.Control type="number" 
                onChange={(e)=>{
                    let intAge = parseInt(e.currentTarget.value);
                    setAge(intAge);
                }}
                placeholder="Enter age" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Occupation</Form.Label>
                <Form.Control type="text" 
                onChange ={(e)=>setOccupation(e.currentTarget.value)}
                placeholder="Enter occupation" />
            </Form.Group>
            <Form.Group controlId="formBasicEmail">
                <Form.Label>Education</Form.Label>
                <Form.Control type="text" 
                onChange= {(e)=>setEducation(e.currentTarget.value)}
                placeholder="Enter education" />
            </Form.Group>
            <Button variant="primary" type="submit">
                Submit
  </Button>
        </Form>
    </>
}

export default SurveyPage;